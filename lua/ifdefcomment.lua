local M = {}

local default_config = {
  add_space = false,
  comment_else = false,

  comment_style = {
    _endif = {
      _if = "",
      _ifdef = "",
      _ifndef = "!",
    },
    _else = {
      _if = "not ",
      _ifdef = "not ",
      _ifndef = "",
    },
  },

  no_cache = false,
}

local function add_user_command()
vim.api.nvim_create_user_command("IfdefComment", function()
  if vim.g.ifdefcomment_config.no_cache then
    print("no_cache true")
    package.loaded.ifdefcomment = nil
    require("ifdefcomment").setup()
  end
  require("ifdefcomment").comment()
end, {})
end

local comments = {}

local function get_comment_space()
  if vim.g.ifdefcomment_config.add_space then
    return " "
  end
  return ""
end

local function populate_comments()
  comments = {
    _endif = {
      _if = get_comment_space() .. vim.g.ifdefcomment_config.comment_style._endif._if,
      _ifdef = get_comment_space() .. vim.g.ifdefcomment_config.comment_style._endif._ifdef,
      _ifndef = get_comment_space() .. vim.g.ifdefcomment_config.comment_style._endif._ifndef,
    },
    _else = {
      _if = get_comment_space() .. vim.g.ifdefcomment_config.comment_style._else._if,
      _ifdef =  get_comment_space() .. vim.g.ifdefcomment_config.comment_style._else._ifdef,
      _ifndef = get_comment_space() .. vim.g.ifdefcomment_config.comment_style._else._ifndef,
    },
  }
end

-- set_options is based on
-- https://github.com/navarasu/onedark.nvim/blob/master/lua/onedark/init.lua
-- Copyright (c) 2021 Navarasu - MIT
function M.set_options(opt, value)
  local cfg = vim.g.ifdefcomment_config
  cfg[opt] = value
  vim.g.ifdefcomment_config = cfg
end

-- setup is based on
-- https://github.com/navarasu/onedark.nvim/blob/master/lua/onedark/init.lua
-- Copyright (c) 2021 Navarasu - MIT
function M.setup(opts)
  if not vim.g.ifdefcomment_config or not vim.g.ifdefcomment_config.loaded then
      vim.g.ifdefcomment_config= vim.tbl_deep_extend(
       'keep',
       vim.g.ifdefcomment_config or {},
       default_config
      )
      M.set_options('loaded', true)
  end
  if opts then
      vim.g.ifdefcomment_config = vim.tbl_deep_extend('force', vim.g.ifdefcomment_config, opts)
  end
  populate_comments()
  add_user_command()
end

local function get_comment(line, token)
    if line:match("^#if%s") then
      return string.gsub(line, "#if%s*", comments["_" .. token]._if)
    elseif line:match("^#ifdef%s") then
      return string.gsub(line, "#ifdef%s*", comments["_" .. token]._ifdef)
    elseif line:match("^#ifndef%s") then
      return string.gsub(line, "#ifndef%s*", comments["_" .. token]._ifndef)
    end
    return nil
end

local function alter_line(stack, line, num, token)
  if #stack == 0 then
    error(token .. " without if at line: " .. num)
    return false
  end
  local comment = get_comment(stack[#stack], token)
  if token == "endif" then
    table.remove(stack, #stack)
  end
  local altered_line = "#" .. token .. " //" .. comment
  if line ~= altered_line then
    vim.api.nvim_buf_set_lines(0, num-1, num, true, {altered_line})
  end
  return true
end

function M.comment()
  local stack = {}
  local lines = vim.api.nvim_buf_get_lines(0, 0, -1, true)
  for num, line in ipairs(lines) do
    -- FIMXE: find out how to use regex or operator with match()
    if line:match("^#if%s") or line:match("^#ifdef%s") or line:match("^#ifndef%s") then
      table.insert(stack, line)
    elseif line:match("^#endif") then
      if not alter_line(stack, line, num, "endif") then
        break;
      end
    elseif line:match("^#else") then
      if vim.g.ifdefcomment_config.comment_else and not alter_line(stack, line, num, "else") then
        break;
      end
    end
  end
  if #stack > 0 then
    error("missing endif: " ..  table.remove(stack, #stack))
  end
end

return M

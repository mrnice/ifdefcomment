# ifdefcomment

Simple neovim ifdef commenter. This plugin takes the (#if|#ifdef|#ifndef) argument and adds it as a comment to the matching #endif or #else (optional).

It does not support #elifdef and #elifdef as those are C++23 and constexpr if is a better replacement anyways.

## Project reasons

This is Bob.

Bob has to fix a lot of old and rusty C and C++ code. People tend to use #ifdef to fix modularization and interface problems.

Bob is unhappy as he cannot change the architecture and must extend those #ifdef's. #ifdef's are kind of hard to read. As most programmers Bob is forced to manually add comments at the end of #endif directives to get a better view of the code structure. And his colleagues also force him to do so. Therefore virtual texts are not a real option as not all colleagues use the fine neovim.

Bob reads tons of source code where the comments are even wrong.

Bob found this plugin and just replaces all of them with the right ones :)

Bob is kind of happy :D

If you are not forced to use comments Bob suggests the fine [vim-matchup](https://github.com/andymass/vim-matchup) plugin.

Bob also plans to write a virtual text #ifdef commenter.

## Example with default configuration

This example code

```C++
#define ABCD 2
#include <stdio.h>
 
int main(void)
{
 
#ifdef ABCD
    printf("1: yes\n");
#else
    printf("1: no\n");
#endif

#ifndef ABCD
    printf("2: no1\n");
#elif ABCD == 2
    printf("2: yes\n");
#else
    printf("2: no2\n");
#endif

#if !defined(DCBA) && (ABCD < 2 * 4 - 3)
    printf("3: yes\n");
#endif

#ifdef CPU
    printf("4: no1\n");
#elifdef GPU
    printf("4: no2\n");
#elifndef RAM
    printf("4: yes\n");
#else
    printf("4: no3\n");
#endif
}
```

Will transform into


```C++
#define ABCD 2
#include <stdio.h>
 
int main(void)
{
 
#ifdef ABCD
    printf("1: yes\n");
#else
    printf("1: no\n");
#endif //ABCD

#ifndef ABCD
    printf("2: no1\n");
#elif ABCD == 2
    printf("2: yes\n");
#else
    printf("2: no2\n");
#endif //!ABCD

#if !defined(DCBA) && (ABCD < 2 * 4 - 3)
    printf("3: yes\n");
#endif //!defined(DCBA) && (ABCD < 2 * 4 - 3)

#ifdef CPU
    printf("4: no1\n");
#elifdef GPU
    printf("4: no2\n");
#elifndef RAM
    printf("4: yes\n");
#else
    printf("4: no3\n");
#endif //CPU
}
```

With comment_else = true

```C++
#define ABCD 2
#include <stdio.h>
 
int main(void)
{
 
#ifdef ABCD
    printf("1: yes\n");
#else //not ABCD
    printf("1: no\n");
#endif //ABCD

#ifndef ABCD
    printf("2: no1\n");
#elif ABCD == 2
    printf("2: yes\n");
#else //ABCD
    printf("2: no2\n");
#endif //!ABCD

#if !defined(DCBA) && (ABCD < 2 * 4 - 3)
    printf("3: yes\n");
#endif //!defined(DCBA) && (ABCD < 2 * 4 - 3)

#ifdef CPU
    printf("4: no1\n");
#elifdef GPU
    printf("4: no2\n");
#elifndef RAM
    printf("4: yes\n");
#else //not CPU
    printf("4: no3\n");
#endif //CPU
}
```

## Commands

| command       | lua function call                     |
| ------------- | ------------------------------------- |
| :IfdefComment | lua require("ifdefcomment").comment() |

## Options

As every developer has a different sort of comment style you are able to change all comment options except the comment it self. It is currently hardcoded as "//".

The default options are:

```lua
{
  add_space = false, -- add a space after //
  comment_else = false, -- comment #else

  comment_style = {
    _endif = { -- comments for the #endif directive, it will be applied to an #endif token
      _if = "", -- this will be applied for an #endif token after // "your_text" text from #if
      _ifdef = "", -- this will be applied for an #endif token after // "your_text" text from #ifdef
      _ifndef = "!", -- this will be applied for an #endif token after // "your_text" text from #ifndef
    },
    _else = { -- comments for the #else directive, it will be applied to an #endif token
      _if = "not ", -- this will be applied for an #else token after // "your_text" text from #if
      _ifdef = "not ", -- this will be applied for an #else token after // "your_text" text from #ifdef
      _ifndef = "", -- this will be applied for an #else token after // "your_text" text from #ifndef
    },
  },
}
```

## Installation

With lazy.nvim

```lua
  {
    "https://gitlab.com/mrnice/ifdefcomment",
    config = function ()
      require("ifdefcomment").setup {
        -- add your setup here or use the default
      }
    end
  }
```

## Todo

* Add an option to support /* */ comments.
* Add a virtual text option, without altering the comments
* Add errors for wrong comments, which are shown by something like trouble.nvim